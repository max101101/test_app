FROM golang

WORKDIR /app/test_app

COPY . .

RUN go build

CMD ["./test_app"]
